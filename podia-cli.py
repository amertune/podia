#!/usr/bin/env python
import click

import podia.podcast as podiapodcast


class AliasedGroup(click.Group):
    def get_command(self, ctx, cmd_name):
        rv = click.Group.get_command(self, ctx, cmd_name)
        if rv is not None:
            return rv
        matches = [x for x in self.list_commands(ctx)
                   if x.startswith(cmd_name)]
        if not matches:
            return None
        elif len(matches) == 1:
            return click.Group.get_command(self, ctx, matches[0])
        ctx.fail('Too many matches: {}'.format(', '.join(sorted(matches))))


@click.group(cls=AliasedGroup)
def cli():
    pass


@cli.group(cls=AliasedGroup)
def podcast():
    pass


@podcast.command()
@click.argument('url')
def subscribe(url):
    podiapodcast.subscribe(url)


@podcast.command()
@click.argument('id')
@click.option('--delete-files/--no-delete-files', default=False)
def unusbscribe(id, delete_files):
    podiapodcast.unsubscribe(id, delete_files)


@podcast.command('list')
def list_podcasts():
    for p in podiapodcast.get_podcasts():
        print('{id}: {title} ({episodes} episodes)'.format(**p))


@podcast.command('update')
@click.argument('id', default=None)
def update_podcasts(id):
    podiapodcast.update(id)


@podcast.group(cls=AliasedGroup)
def episode():
    pass


@episode.command('list')
@click.argument('id', required=False, default=None)
def list_episodes(id):
    for ep in podiapodcast.get_episodes(id):
        print('{id}: {title}'.format(**ep))


@episode.command('delete')
@click.argument('id')
def delete_episode(id):
    podiapodcast.delete_episode(id)

@episode.command('download')
@click.argument('id')
def download_episode(id):
    podiapodcast.download_episode(id)


if __name__ == '__main__':
    cli()
