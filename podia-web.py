#!/usr/bin/env python
from bottle import route, run

from podia import podcast

@route('/')
def root():
    return '<html><body><h1>Podcasts</h1><div>' + '<br>'.join('{id}: {title} ({episodes})'.format(**x) for x in podcast.get_podcasts()) + '</div></body></html>'

run(host='localhost', port=8080, debug=True)