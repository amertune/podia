import os

from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

from . import config

os.makedirs(config['data_dir'], exist_ok=True)
engine = create_engine('sqlite:///{}/podia.db'.format(config['data_dir']))
Base = declarative_base()


class Podcast(Base):
    __tablename__ = 'podcast'

    id = Column(Integer, primary_key=True)
    url = Column(String, unique=True)
    title = Column(String)
    description = Column(String)
    link = Column(String)
    cover_url = Column(String)
    paged_feed_next = Column(String)

    episodes = relationship('Episode', cascade='all, delete-orphan')


class Episode(Base):
    __tablename__ = 'episode'

    id = Column(Integer, primary_key=True)
    guid = Column(String, unique=True)
    podcast_id = Column(Integer, ForeignKey('podcast.id'), nullable=False)
    title = Column(String)
    subtitle = Column(String)
    description = Column(String)
    published = Column(DateTime)
    total_time = Column(Integer)

    media_url = Column(String)
    media_size = Column(Integer)
    media_mimetype = Column(String)

    podcast = relationship('Podcast')

    deleted = Column(Integer)

Base.metadata.create_all(engine)

session = sessionmaker(bind=engine)()
