import appdirs
import configparser
import os
import sys

class Config:
    """
    Used to access configuration settings. Config will also read and write configuration files.
    """
    def __init__(self):
        self.appname = 'podia'
        self.appauthor = 'jshipley'

        self._config_dir = appdirs.user_config_dir(self.appname, self.appauthor)
        self._config_file = os.path.join(self._config_dir, 'config')

        self._config = configparser.ConfigParser()
        if os.path.isfile(self._config_file):
            self._config.read(self._config_file)
        else:
            self._config['DEFAULT']['data_dir'] = appdirs.user_data_dir(self.appname, self.appauthor)
            self.write()

    def write(self):
        """
        Write configuration settings to configuration file
        """
        os.makedirs(self._config_dir, exist_ok=True)
        with open(self._config_file, 'w') as outfile:
                self._config.write(outfile)

    def __getitem__(self, key):
        """
        Get configuration settings that are either defined in this class or in the config file
        :param key: the configuration setting to return
        :return: configuration setting
        """
        if key in self.__dict__:
            return self.__dict__[key]
        return self._config['DEFAULT'][key]

    def __setitem__(self, key, value):
        self._config['DEFAULT'][key] = value

# replace this module with an instance of the Config class
sys.modules[__name__] = Config()