import contextlib
import datetime
import os.path
import podcastparser
import shutil
import urllib.request

from . import config
from . import database


def subscribe(feed_url):
    """Reads podcast feed from feed_url and creates a new subscription."""

    normalized_url = podcastparser.normalize_feed_url(feed_url)
    parsed_podcast = podcastparser.parse(normalized_url, urllib.request.urlopen(normalized_url))

    podcast = database.Podcast(title=parsed_podcast['title'], url=normalized_url)
    database.session.add(podcast)

    podcast.episodes.extend(database.Episode(**{
       'guid': ep['guid'],
       'title': ep['title'],
       'subtitle': ep['subtitle'],
       'description': ep['description'],
       'published': datetime.datetime.fromtimestamp(ep['published']),
       'total_time': ep['total_time'],
       'media_url': ep['enclosures'][0]['url'],
       'media_size': ep['enclosures'][0]['file_size'],
       'media_mimetype': ep['enclosures'][0]['mime_type']
    }) for ep in parsed_podcast['episodes'])
    database.session.commit()


def update(podcast_id=None, max_pages=1):
    """
    Update the podcast feed(s) and retrieve information for all new episodes

    :param podcast_id: only update the specified podcast
    :param max_pages: For paged feeds, the number of pages to retrieve
    """
    if podcast_id is not None:
        podcasts = [database.session.query(database.Podcast).get(podcast_id)]
    else:
        podcasts = database.session.query(database.Podcast).all()

    for podcast in podcasts:
        parsed_podcast = podcastparser.parse(podcast.url, urllib.request.urlopen(podcast.url))

        podcast.title = parsed_podcast['title']
        podcast.description = parsed_podcast['description']
        podcast.link = parsed_podcast['link']
        podcast.cover_url = parsed_podcast['cover_url']
        if 'paged_feed_next' in parsed_podcast:
            podcast.paged_feed_next = parsed_podcast['paged_feed_next']

    for ep in parsed_podcast['episodes']:
        if not database.session.query(database.Episode).filter_by(guid=ep['guid']).one_or_none():
            podcast.episodes.append(database.Episode(**{
               'guid': ep['guid'],
               'title': ep['title'],
               'subtitle': ep['subtitle'],
               'description': ep['description'],
               'published': datetime.datetime.fromtimestamp(ep['published']),
               'total_time': ep['total_time'],
               'media_url': ep['enclosures'][0]['url'],
               'media_size': ep['enclosures'][0]['file_size'],
               'media_mimetype': ep['enclosures'][0]['mime_type']
            }))
    database.session.commit()


def unsubscribe(podcast_id, delete=False):
    """
    Remove podcast subscription and all episodes

    :param delete: if delete is true then also delete all downloaded episodes
    """
    podcast = database.session.query(database.Podcast).get(podcast_id)
    if delete:
        shutil.rmtree(get_podcast_local_path(podcast))

    database.session.delete(podcast)
    database.session.commit()


def get_podcasts():
    """:return: a generator containing all subscribed podcasts"""
    return ({'id': p.id, 'title': p.title, 'episodes': len(p.episodes)}
            for p in database.session.query(database.Podcast).all())

def get_podcast_local_path(podcast):
    """
    :param podcast: the podcast db object
    :return: the path in the local data dir where this podcast's episodes are stored
    """
    return os.path.join(config['data_dir'], 'podcasts', podcast.title)

def get_episodes(podcast_id=None):
    """
    :param podcast_id: only return episodes for the specified podcast
    :return: a generator of podcast episodes
    """
    query = database.session.query(database.Episode)
    if podcast_id:
        query = query.filter_by(podcast_id=podcast_id)
    return ({'id': e.id, 'title': e.title} for e in query.all() if not e.deleted)


def get_deleted_episodes(podcast_id=None):
    """
    :param podcast_id: only return episodes for the specified podcast
    :return: a generator of deleted podcast episodes
    """
    query = database.session.query(database.Episode)
    if podcast_id:
        query = query.filter_by(podcast_id=podcast_id)
    return ({'id': e.id, 'title': e.title} for e in query.all() if e.deleted)


def download_episode(episode_id):
    """
    Download the audio file associated with the podcast episode

    :param episode_id:
    """
    episode = database.session.query(database.Episode).get(episode_id)

    media_file = get_episode_local_path(episode)
    os.makedirs(os.path.dirname(media_file), exist_ok=True)
    if not os.path.exists(media_file):
        with urllib.request.urlopen(episode.media_url) as response, open(media_file, 'wb') as out_file:
            shutil.copyfileobj(response, out_file)


def delete_episode(episode_id):
    """
    Delete the episode information and the associated audio file

    :param episode_id:
    """
    episode = database.session.query(database.Episode).get(episode_id)

    media_file = get_episode_local_path(episode)
    with contextlib.suppress(FileNotFoundError):
        os.remove(media_file)

    episode.deleted = True
    database.session.commit()


def undelete_episode(episode_id):
    """
    Remove the deleted flag from an episode
    :param episode_id:
    """
    episode = database.session.query(database.Episode).get(episode_id)
    episode.deleted = False
    database.session.commit()


def get_episode_local_path(episode):
    """
    Append episode filename to the configured storage path

    :param episode: an episode db object
    :return: an absolute path where the audio file should live
    """
    # TODO: use configuration setting to rename file, and save renamed file in database
    return os.path.join(config['data_dir'],
                        'podcasts',
                        episode.podcast.title,
                        os.path.basename(episode.media_url))


def sync(podcast_dir, podcast_id=None):
    """
    Copy new episodes to podcast_dir

    :param podcast_dir: a path on the filesystem
    :param podcast_id: only sync the episodes for the specified podcast
    """
    pass

