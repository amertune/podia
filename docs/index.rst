.. podia documentation master file, created by
   sphinx-quickstart on Mon Jun  6 10:41:00 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to podia's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 4

   podcast


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

